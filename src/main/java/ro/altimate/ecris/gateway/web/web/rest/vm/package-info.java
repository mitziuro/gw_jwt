/**
 * View Models used by Spring MVC REST controllers.
 */
package ro.altimate.ecris.gateway.web.web.rest.vm;
