package ro.altimate.ecris.gateway.web.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import ro.altimate.ecris.gateway.web.domain.Authority;

/**
 * Spring Data MongoDB repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends ReactiveMongoRepository<Authority, String> {}
